# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 14:15:05 2018

@author: liufengming

Modified by Gaofei in usage of bachelor thesis
"""

from concurrent import futures
import tensorflow as tf
import time
import grpc
import DNNPartition.DNN_pb2 as DNN_pb2
import DNNPartition.DNN_pb2_grpc as DNN_pb2_grpc
import engine

####################Session####################
class Servicer(DNN_pb2_grpc.CloudExecutionServicer):
  def execute_on_cloud(self, request, context): 
    #The name of the func must be exactly the same as that defined in the proto filters
    response = DNN_pb2.ExeResponse()
    response.predictions.extend(engine.execute_on_cloud(model_name=request.model_name, 
                                                          batch_size=request.batch_size,
                                                          node_list=request.node_list, 
                                                          data_list=request.data_list, 
                                                          image_shape=request.image_shape))
    return response

def serve():
  	# create a gRPC server
  	server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

  	# add the method defined above into the server
  	DNN_pb2_grpc.add_CloudExecutionServicer_to_server(Servicer(), server)

  	# run the server at port 50051
  	service_port = 50051
  	server.add_insecure_port('[::]:{}'.format(service_port))
  	server.start()

  	print("The server starts at port: {}".format(service_port))
  	try:
    		while True:
    			time.sleep(86400)
  	except KeyboardInterrupt:
      		print("The server terminates!")
      		server.stop(0)

if __name__ == '__main__':
	serve()