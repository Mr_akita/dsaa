# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 14:18:40 2018

@author: liufengming

Modified by Gaofei in usage of bachelor thesis
"""

import tensorflow as tf
import psutil
import pandas as pd
import numpy as np
import copy
import time
import grpc
from DNNPartition import DNN_pb2
import DNNPartition.DNN_pb2_grpc as DNN_pb2_grpc
import pickle
import matplotlib.image as mplimg
import os
import ntpath
import imp
from collections import defaultdict

def obtain_bandwidth():
    WLAN_info = psutil.net_io_counters(pernic=True)['h1-eth0'] #wlp2s0']
# WLAN origin
    print("WLAN_info: ", WLAN_info[0], "\n")
    return int(WLAN_info[0]/102400 + 1)


def BFS(graph, start, end):
    # Mark all the vertices as not visited
    visited = dict.fromkeys(graph.keys(), False)
    parent = dict.fromkeys(graph.keys(), -1)

    # Enqueue the start node and mark it
    queue = [start]
    visited[start] = True

    while queue:
        cur = queue.pop(0)
        if cur==end:
            break
        for child, cost in graph[cur].items():
            if (not visited[child]) and (cost>0):
                queue.append(child)
                visited[child] = True
                parent[child] = cur
    return parent if parent[end]!=-1 else None



def local_FIND(graph, source, sink):
    org_graph = copy.deepcopy(graph)
    max_flow = 0 # There is no flow initially
    parent = BFS(graph=graph, start=source, end=sink)

    while (parent):
        #print(graph)
        # Find minimum residual capacity of the edges along the
        # path filled by BFS. Or we can say find the maximum flow
        # through the path found.
        path_flow = float("Inf")
        # Traverse from sink to source
        cur = sink
        while(cur !=  source):
            # compare the link parent[s]-s with path_flow and select
            # the min
            path_flow = min (path_flow, graph[parent[cur]][cur])
            # shift the cursor backwards 1 link
            cur = parent[cur]

        # Add path flow to overall flow
        max_flow +=  path_flow

        # Update residual capacities of the edges and reverse edges
        # along the path
        v = sink
        while(v !=  source):
            u = parent[v]
            # Extract the flow from the forward
            graph[u][v] -= path_flow
            # Extract the neg-flow from the reversed
            # no need for DAG
            # graph[v][u] += path_flow
            # shift the cursor backwards 1 link
            v = parent[v]
        parent = BFS(graph=graph, start=source, end=sink)

    min_cut = []
    source_set = set([])
    sink_set = set([])
    # print the edges which initially had weights
    # but now have 0 weight
    for node, outlinks in graph.items():
        for successor, cost in outlinks.items():
            if cost == 0 and org_graph[node][successor] > 0:
               min_cut.append((node, successor))
               if node!='e':
                   source_set.add(node)
               if successor!='c':
                   sink_set.add(successor)

    return list(source_set), list(sink_set)
"""
def DFS(graph, u, parent, iscut_list, parent_list, dfs_clock, pre):
    child = 0
    dfs_clock = dfs_clock + 1
    low_u = dfs_clock
    pre[u] = dfs_clock
"""

class Undirected_Graph: 
   
    def __init__(self,vertices): 
        self.V= vertices #No. of vertices 
        self.graph = defaultdict(list) # default dictionary to store graph 
        self.Time = 0
   
    # function to add an edge to graph 
    def addEdge(self,u,v): 
        self.graph[u].append(v) 
        self.graph[v].append(u) 
   
    '''A recursive function that find articulation points  
    using DFS traversal 
    u --> The vertex to be visited next 
    visited[] --> keeps tract of visited vertices 
    disc[] --> Stores discovery times of visited vertices 
    parent[] --> Stores parent vertices in DFS tree 
    ap[] --> Store articulation points'''
    def DFS(self, u, visited, ap, parent, low, disc): 
  
        #Count of children in current node  
        children =0
  
        # Mark the current node as visited and print it 
        visited[u]= True
  
        # Initialize discovery time and low value 
        disc[u] = self.Time 
        low[u] = self.Time 
        self.Time += 1
  
        #Recur for all the vertices adjacent to this vertex 
        for v in self.graph[u]: 
            # If v is not visited yet, then make it a child of u 
            # in DFS tree and recur for it 
            if visited[v] == False : 
                parent[v] = u 
                children += 1
                self.DFS(v, visited, ap, parent, low, disc) 
  
                # Check if the subtree rooted with v has a connection to 
                # one of the ancestors of u 
                low[u] = min(low[u], low[v]) 
  
                # u is an articulation point in following cases 
                # (1) u is root of DFS tree and has two or more chilren. 
                if parent[u] == -1 and children > 1: 
                    ap[u] = True
  
                #(2) If u is not root and low value of one of its child is more 
                # than discovery value of u. 
                if parent[u] != -1 and low[v] >= disc[u]: 
                    ap[u] = True    
                      
                # Update low value of u for parent function calls     
            elif v != parent[u]:  
                low[u] = min(low[u], disc[v]) 
  
  
    #The function to do DFS traversal. It uses recursive APUtil() 
    def AP(self): 
   
        # Mark all the vertices as not visited  
        # and Initialize parent and visited,  
        # and ap(articulation point) arrays 
        visited = [False] * (self.V) 
        disc = [float("Inf")] * (self.V) 
        low = [float("Inf")] * (self.V) 
        parent = [-1] * (self.V) 
        ap = [False] * (self.V) #To store articulation points 

        cut_set = set([])
        # Call the recursive helper function 
        # to find articulation points 
        # in DFS tree rooted with vertex 'i' 
        for i in range(self.V): 
            if visited[i] == False: 
                self.DFS(i, visited, ap, parent, low, disc) 
  
        for index, value in enumerate (ap): 
            if value == True: 
                cut_set.add(index)
        return list(cut_set)

def Tarjan(graph, source, sink):
    org_graph = Undirected_Graph(graph.shape[0])
    i = 0
    for x in range(len(graph)):
        for y in range(len(graph[x])):
            if (graph[x][y]):
                org_graph.addEdge(x, y)
    return org_graph.AP()

"""
    for x in graph:
        for y in x:

    V_cut = {}
    dfs_clock = 0
    iscut_list = dict.fromkeys(graph.keys(), False)
    parent_list = dict.fromkeys(graph.keys(), -1)
    pre = dict.fromkeys(graph.keys(), -1)
    DFS(graph, source, -1, iscut_list, parent_list, dfs_clock, pre)

    for v in iscut_list:
"""
def graph_construct(graph, V_cut, i):
    u = V_cut[i]
    v = V_cut[i + 1]
    temp_graph = {}
    temp_graph[u] = {}

    visited = dict.fromkeys(graph.keys(), False)
    parent = dict.fromkeys(graph.keys(), -1)

    queue = [u]
    visited[u] = True

    while queue:
        cur = queue.pop(0)
        if cur==v:
            break
        for child, cost in graph[cur].items():
            if (not visited[child]) and (cost>0):
                queue.append(child)
                visited[child] = True
                temp_graph[cur] = {child: cost}
    return temp_graph, u, v


def Find_min(graph, source, sink):
    delay_min = float("Inf")
    V_e = set([])
    V_c = set([])

    V_cut = Tarjan(graph, source, sink)
    cut_num = len(V_cut)

    for i in range(cut_num + 1):
        temp, u, v = graph_construct(graph, V_cut, i)
        temp_source_set, temp_sink_set
        = local_FIND(temp, u, v)
        delay_temp = 0
        for j in temp_source_set:
            for k in temp_sink_set:
                if temp[j][k]:
                    delay_temp += temp[j][k]

        for j in temp_source_set:
            delay_temp += graph['e'][j]
        for j in temp_sink_set:
            delay_temp += graph[j]['c']
        if delay_temp < delay_min:
            delay_min = delay_temp
            V_e = temp_source_set
            V_c = temp_sink_set
    return list(V_e), list(V_c)

def cal_partition(profile):
    B = obtain_bandwidth()
    vertice_num = profile.shape[0]
    for i in range(vertice_num):
        profile['successors'][i] = profile['successors'][i].split(",")

    #####Compute net
    F_trans = []
    for data_size in profile['data_size']:
        F_trans.append(data_size/B)
    profile.insert(loc=profile.shape[1], column="transmission_time", value=F_trans)
    del F_trans

    #####Graph construct
    cost_profile = {}
    cost_profile['e'] = {}
    cost_profile['c'] = {}

    for i in range(vertice_num):
        cur_node = profile['node'][i]

        # edge link
        cost_profile[cur_node] = {'c': profile['edge_time'][i]}

        # cloud link
        cost_profile['e'][cur_node] = profile['cloud_time'][i]

        if len(profile['successors'][i])>1:
            # add aux
            aux_node = cur_node + "_aux"
            cost_profile[aux_node] = {}
            cost_profile[cur_node][aux_node] = profile['transmission_time'][i]

            for successor in profile['successors'][i]:
                cost_profile[aux_node][successor] = np.inf
        elif profile['successors'][i][0]!='output':
            cost_profile[cur_node][profile['successors'][i][0]] = profile['transmission_time'][i]

    #####Min cut
    return Find_min(graph=cost_profile, source='e', sink='c') # origin version

def get_images(dataset_folder):
    images = []
    for root, dirs, files in os.walk(dataset_folder):
        for file in files:
            images.append(mplimg.imread(os.path.join(root, file)))
    return images

def PartitionRun(dataset_folder, batch_size, model_file, profile, cloud_process):
    print("##########  Edge  ##########")
    # Load data
    tic = time.time()
    images = get_images(dataset_folder)
    dataset_size = len(images)
    image_shape = images[0].shape
    print("Load data: {} ms".format((time.time() - tic)*1000))

    # Load graph
    tic = time.time()
    NN_model = imp.load_source(ntpath.basename(model_file).split(".")[0], model_file) #very slow
    graph, graph_name, input_data, output, match_table = NN_model.model(image_shape=image_shape, batch_size=batch_size)
    print("Load graph: {} ms".format((time.time() - tic)*1000))

    # Start session
    with tf.Session(graph=graph) as sess:
        tic = time.time()
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        print("Edge Graph Initialization: {} ms".format((time.time() - tic)*1000))

        # Batchwise execution
        prediciton_per_batch = []
        for i in range(int((dataset_size-1)/batch_size)+1):
            # Get one batch of data
            batch = np.array(images[i*batch_size: (i+1)*batch_size]) #assume dataset_size is a multiple of batch_size
            print()
            print("----- Batch", i, "-----")

            # Get the partition point
            tic = time.time()
            edge_node_list, cloud_node_list = cal_partition(profile=pd.read_csv(profile))
            edge_node_list.append("input")
            print("Partition: {} ms".format((time.time() - tic)*1000))
            print("Nodes to be executed on edge:", edge_node_list)


            #On edge
            cloud_data_list = []
            tic = time.time()
            for node in edge_node_list:
                result = sess.run(fetches=match_table[node],
                                  feed_dict={input_data: np.reshape(a=batch,
                                                                    newshape=(batch_size, image_shape[0], image_shape[1], image_shape[2]))})
                result = pickle.dumps(result)
                data = DNN_pb2.Data()
                data.values = result
                cloud_data_list.append(data)
            print("Edge Inference: {} ms".format((time.time() - tic)*1000))

            #On cloud
            print("##########  Cloud  ##########")
            with grpc.insecure_channel(cloud_process) as channel:
                stub = DNN_pb2_grpc.CloudExecutionStub(channel)
                request = DNN_pb2.ExeRequest(model_name=graph_name,
                                             image_shape=image_shape,
                                             node_list=edge_node_list,
                                             data_list=cloud_data_list,
                                             batch_size=batch_size)
                response = stub.execute_on_cloud(request)
                prediciton_per_batch.append(response.predictions)

    return prediciton_per_batch
