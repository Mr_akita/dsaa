# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 14:15:37 2018

@author: liufengming

Modified by Gaofei in usage of bachelor thesis
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Imports
import tensorflow as tf
import pickle
import time
import imp


def execute_on_cloud(model_name, batch_size, node_list, data_list, image_shape):
    # Load graph
    tic = time.time()
    NN_model = imp.load_source(model_name, "./model_collection/"+model_name+".py") #very slow
    graph, graph_name, input_data, output, match_table = NN_model.model(image_shape=image_shape, batch_size=batch_size)
    print("Load graph: {} ms".format((time.time() - tic)*1000))
    
    # Load data
    tic = time.time()
    cloud_feed_dict = {}
    for i in range(len(node_list)):
        node = match_table[node_list[i]]
        cloud_feed_dict[node] = pickle.loads(data_list[i].values)
    print("Load data: {} ms".format((time.time() - tic)*1000))
    
    # On cloud
    with tf.Session(graph=graph) as sess:
        tic = time.time()
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        print("Cloud--Initialization: {} ms".format((time.time() - tic)*1000))
        
        tic = time.time()
        result = sess.run(fetches=output, feed_dict=cloud_feed_dict)
        print("Cloud--Execution: {} ms".format((time.time() - tic)*1000))
        
        return result