# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 14:10:33 2018

@author: liufengming

Modified by Gaofei in usage of bachelor thesis
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Imports
import DNNPartition
import sys

if __name__ == "__main__":
    predictions = DNNPartition.PartitionRun(dataset_folder=sys.argv[1],
                                            batch_size=int(sys.argv[2]),
                                            model_file=sys.argv[3],
                                            profile=sys.argv[4],
                                            cloud_process="10.0.0.2:50051") #localhost:50051
    print()
    print("Predictions :", predictions)
