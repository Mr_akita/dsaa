# -*- coding: utf-8 -*-
"""
Created on Mon Oct  8 14:18:31 2018

@author: liufengming
"""

from distutils.core import setup

setup(name='DNNPartition',
      version='1.0',
      description='The package for automatically partitioning DNN at runtime',
      url='http://github.com/wodeusername',
      author='liufengming',
      author_email='feng-ming.liu@connect.polyu.hk',
      license='None',
      packages=['DNNPartition'],
      zip_safe=False)