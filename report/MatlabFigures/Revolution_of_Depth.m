fun = @(x, y) bar(x,y,0.5);
x = [1:6];
    %'ResNet', 'GoogLeNet', 'VGG', 'ILSVRC_13', 'AlexNet', 'ILSVRC_11'];
y = [3.57, 6.7, 7.3, 11.7, 16.4, 25.8];
z = [152, 22, 19, 8, 8, 1];
[a, b, c]=plotyy(x, y, x, z, fun, 'plot');
%bar(x,y), plot(x,z), ylabel('top-5 error'),
%title('Revolution of Depth')
%print -deps graph.eps
set(c, 'color', [1,0,0], 'LineWidth', 2, 'Marker', 'o', 'MarkerSize', 3);
set(gca, 'xticklabel', {'ResNet', 'GoogLeNet', 'VGG', 'ILSVRC13', 'AlexNet', 'ILSVRC11'});
set(b, 'Facecolor', [0,0.7,0.8]);
ylabel('top-5 error(%)');
ylabel(a(2), 'Layers');